<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Libro;
use App\Categoria;
use Alert;
class Muestralibro extends Controller
{

    public function mostrar(Request $req){

        $libros=Libro::where('nombre', 'Like', "%$req->nombre%")->paginate(5);
        return view('Libros')->with('libros',$libros);
    }
    public function mostrarcat ($id){
        $libro=Libro::find($id);
        $cate = Categoria::find($libro->idcategoria);
        return $cate->nombre;
    }

    public function destroy($id){
        $libro = Libro::find($id);
        $libro->delete();

        Alert::success('Libro eliminado exitosamente');
        return redirect(route('lista'));
    }
    public function editar($id){
        $libro = Libro::find($id);
        $categorias = Categoria::all();
        return view('edit')->with('libro',$libro)->with('categorias',$categorias);
    }

    public function update(Request $req, $id){

        $libro = Libro::find($id);
        $libro->nombre = $req->nombre;
        $libro->autor = $req->autor;
        $libro->fecha = $req->fecha;
        $libro->idcategoria = $req->idcategoria;
        $libro->save();
        Alert::success('Libro modificado exitosamente');
        return redirect(route('lista'));
    }
    public function prestar( Request $req){
        $libro = Libro::find($req->id);
        $libro->status = 1;
        $libro->usuario=$req->usuario;
        $libro->save();

        Alert::basic('Libro prestado','Exito!')->persistent('Listo!');
        return redirect(route('lista'));

    }
    /** */
    public function regresar( Request $req){
        $libro = Libro::find($req->id);
        $libro->status = 0;
        $libro->save();

        Alert::basic('Listo','Libro regresado');
        return redirect(route('lista'));
    }
}
