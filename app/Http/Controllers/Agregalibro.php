<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Libro;
use App\Categoria;
use Alert;
class Agregalibro extends Controller
{

    protected function crear(Request $data)
    {
        $this->validate($data,[
            'nombre' => 'required',
            'fecha' => 'required',
            'idcategoria' => 'required',
            'autor' => 'required',
        ]);
        Libro::create($data->all());
        Alert::success('Libro agregado exitosamente');
        $categorias = Categoria::all();
        return view('agregalib')->with('categorias',$categorias);
    }

    public function mostrar(){
        $categorias = Categoria::all();
        return view('agregalib')->with('categorias',$categorias);
    }
    
    


}
