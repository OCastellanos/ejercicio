<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'autor', 'fecha','idcategoria','status','idusuario'
    ];

    public function scopeNombre($query, $name)
    {
        if ($name != "") {
            $query->where('nombre', 'Like', "%$name%");
        }
    }

    public function categoria()
    {

        return $this->belongsTo('App\Categoria','idcategoria');
    }
}
