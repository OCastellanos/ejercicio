@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Editar el libro ') }}"{{$libro ->nombre}}"</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('edit',$libro ->id) }}">
                            @csrf
                            @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors -> all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="nombre">Nombre</label>
                                    <input value="{{$libro->nombre}}" type="nombre" class="form-control" id="nombre" placeholder="Nombre "name="nombre" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="autor">Autor</label>
                                    <input type="autor" class="form-control" id="autor" placeholder="Autor" name="autor" value="{{$libro->autor}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="fecha">Fecha de publicacion</label>
                                    <input type="date" class="form-control" id="fecha" placeholder="Fecha de publicacion" name = "fecha" value={{$libro->fecha}}>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="id">Categoria</label>
                                    <select id="id" class="form-control" name="idcategoria" value={{$libro->id}}>
                                        @foreach($categorias as $categoria)
                                            <option value={{$categoria->id}}>{{$categoria->nombre}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <button type="submit" class="btn btn-light border-light">Editar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
