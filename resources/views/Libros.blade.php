@extends("layouts.app")

@section("content")
    <div class="container - container-fluid">
        <h2 class="title m-b-md">Lista de libros</h2>
        <form role="search "class="navbar-form navbar-left float-right" method="get" action={{route("lista")}} >
            <div class="form-group">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="..." aria-label="nombre" aria-describedby="nombre" name="nombre" id="nombre">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Buscar</button>
                    </div>
                </div>

            </div>


        </form>
        <br><br><br>
        @foreach($libros as $libro)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{$libro -> nombre}}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{$libro -> autor}}</h6>
                    <p class="card-text">Fecha de publicacion: {{$libro -> fecha}}</p>
                    <p class="card-text">Categoria: {{$cat = App\Categoria::find($libro->idcategoria) -> nombre}}</p>
                    @if($libro -> status==1)
                        <p class="card-text">Status: Prestado</p>
                        <p class="card-text">Usuario: {{$libro -> usuario}}</p>
                        <a href="" class="btn btn-light" data-id={{$libro->id}} data-toggle="modal" data-target="#regresar">Regresar libro</a>
                    @else
                        <p class="card-text">Status: Libre</p>
                        <button class="btn btn-light" data-id={{$libro->id}}  data-toggle="modal" data-target="#prestar">Prestar</button>
                    @endif
                    <a href="{{route('editar',$libro ->id)}}" class="btn btn-info ">Editar</a>
                    <a href="{{route('eliminar',$libro ->id)}}" onclick="return confirm('¿Desea eliminar este registro?')" class="btn btn-danger" >Eliminar</a>
                </div>



            </div>
        @endforeach
    <div class="text-center">
        {!! $libros->links(); !!}
    </div>



        <div class="modal fade" id="prestar" tabindex="-1" role="dialog" aria-labelledby="prestar" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="prestar">Prestar libro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form  method="POST" action={{route("presto")}}>
                            <div class="form-group">

                                @csrf
                                <input type="hidden" name="id" id="id" value="">
                                <label for="">Nombre del usuario</label>
                                <input type="text" class="form-control" id="usuario" placeholder="Usuario" name="usuario">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-light">Aceptar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




        <div class="modal fade" id="regresar" tabindex="-1" role="dialog" aria-labelledby="regresar" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="regresar">Regresar libro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ¿Desea regresar este libro?
                        <form  method="POST" action={{route("regresar",'libro')}}>
                            <div class="form-group">
                                @csrf
                                <input type="hidden" name="id" id="id" value="">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-light">Aceptar</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection