<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');


Route::get('/home', function () {
    return route('home');
});
Route::get('libros','Muestralibro@mostrar')->name('lista');

Route::post('agregarlibro/agrega','Agregalibro@crear')->name('agrega');

Route::post('libros','Muestralibro@prestar')->name('presto');
Route::post('libros/{id}/edit/listo','Muestralibro@update')->name('edit');
Route::post('libros,regresado','Muestralibro@regresar')->name('regresar');
Route::get('agregarlibro', 'Agregalibro@mostrar')->name('agregarlibro');


Route::get('libros/{id}/destroy',[
    'uses' => 'Muestralibro@destroy'
])->name('eliminar');
Route::get('libros/{id}/edit',[
    'uses' => 'Muestralibro@editar'
])->name('editar');
